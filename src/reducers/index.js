import {combineReducers } from "redux";
import ProductReducers from "./ProductReducers";
import OrderReducers from "./OrderReducers";
import {reducer as reducerForm } from "redux-form"; 

const rootReducers = combineReducers({
    orders: OrderReducers,
    products: ProductReducers,
    form: reducerForm
})

export default rootReducers;