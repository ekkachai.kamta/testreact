import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";

class ProductForm extends Component {
    render() {
        return (
            <div>
                <form>
                    <Field name="productName" type="text" Component="input"/>
                </form>
            </div>
        )
    }
}

ProductForm = reduxForm({ form: "productForm"})(ProductForm);

export default ProductForm;
