import './App.css';
import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Home from "./contianers/Home";
import About from "./contianers/About";
import Order from "./contianers/order/Order";
import Product from "./contianers/product/Product";
import Notfound from "./contianers/error/Notfound";
import ProductEdit from "./contianers/product/ProductEdit"

class App extends Component {

 renderRouter(){
   return(
     <Switch>
       <Route exact path="/" component={Home}></Route>
       <Route exact path="/about" component={About}></Route>
       <Route exact path="/order" component={Order}></Route>
       <Route exact path="/product" component={Product}></Route>
       <Route exact path="/product/add" component={ProductEdit}></Route>
       <Route exact path="/product/edit/:id" component={ProductEdit}></Route>
       

       <Route component={Notfound}></Route>
     </Switch>
   )
 }

  render() {
    return (
      <BrowserRouter>{this.renderRouter()}</BrowserRouter>
    );
  }
}

export default App;
