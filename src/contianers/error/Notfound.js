import React from 'react';
import Header from "../../components/Header";
import Footer from "../../components/Footer";

const Notfound = () => {
    return (
        <div>
            <Header />
            <div>
                <h1>404</h1>
            </div>
            <Footer />
        </div>
    )
}
export default Notfound;